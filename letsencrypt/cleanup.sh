#!/bin/bash

the_repo="gitlab.com/CommonsPub/commonspub.gitlab.io.git"

git rm $CI_PROJECT_DIR/static/.well-known/acme-challenge/*
git commit -m "GitLab runner - auto-removed certbot challenge files"
git push https://$GITLAB_USER_LOGIN:$CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN@$the_repo HEAD:master
