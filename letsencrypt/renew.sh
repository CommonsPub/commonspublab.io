#!/bin/bash

the_domain="commonspub.org"

echo "Checking age of certificate for $the_domain ..."
end_epoch=$(date -d "$(echo | openssl s_client -connect $the_domain:443 -servername $the_domain 2>/dev/null | openssl x509 -enddate -noout | cut -d'=' -f2)" "+%s")

current_epoch=$(date "+%s")
renew_days_threshold=30
days_diff=$((($end_epoch - $current_epoch) / 60 / 60 / 24))

if [ $days_diff -lt $renew_days_threshold ]; then

  echo "Certificate is $days_diff days old, renewing now."
  
  certbot certonly --manual --debug --preferred-challenges=http -m $GITLAB_USER_EMAIL --agree-tos --manual-auth-hook authenticator.sh --manual-cleanup-hook cleanup.sh --manual-public-ip-logging-ok -d $the_domain

  echo "Certbot finished. Updating the certificate via GitLab Pages API."
  curl --request PUT --header "PRIVATE-TOKEN: $CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN" --form "certificate=@/etc/letsencrypt/live/$the_domain/fullchain.pem" --form "key=@/etc/letsencrypt/live/$the_domain/privkey.pem" https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pages/domains/$the_domain

else
  echo "Certificate still valid for $days_diff days, no renewal required."
fi